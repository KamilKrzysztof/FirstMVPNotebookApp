package mvp_sqlite.training.com.firstmvpnotebook.mvp;


import java.util.List;

import mvp_sqlite.training.com.firstmvpnotebook.pojo.Note;

public interface NotesViewInterface {
    void showAllElementList(List<Note> notesList);
    String getTextFromEditText();
}

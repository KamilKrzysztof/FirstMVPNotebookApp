package mvp_sqlite.training.com.firstmvpnotebook.mvp;


public interface NotesPresenterInterface {
    void onButtonClicked();
    void onListItemClicked(int position);
    void onListItemLongClicked(int position);
    void refreshDatabaseElements();
}
